<?php
  // Connection au server localhost, bdd : cfa
  function connexion()
  {
    $con = mysqli_connect('localhost', 'root', '', 'cfa');
    if(!$con)
    {
      echo "erreur de connection au server";
      return null;
    }
    return $con;
  }

  // deconnexion du server
  function deconnexion($con)
  {
    mysqli_close($con);
  }

  // Lister les classes
  function select_All_classes()
  {
    $requete = "select * from classe;";
    $con = connexion();
    if ($con != null)
    {
      $resultats = mysqli_query($con, $requete);
      echo "<table class=\"custom\" border=1>
            <tr><thead><td>Id Classe</td>
                <td>Désignation</td>
                <td>Salle</td>
                <td>Action</td></thead>
            </tr>
      ";

      // Parcours des résultats
      while ($ligne = mysqli_fetch_assoc($resultats))
      {
        echo "<tr><td>",$ligne['idclasse'],"</td>
                  <td><a href='index.php?page=1&action=e&idclasse=".$ligne['idclasse'],"'>".$ligne['designation']."</a></td>
                  <td>",$ligne['salle'],"</td>
                  <td><a href='index.php?page=1&action=x&idclasse=".$ligne['idclasse']."'> <i class=\"far fa-times-circle\"></i> </a></td></tr>";
      }
      echo "<table>";
      deconnexion($con);
    }
  }

  function afficher_classes()
  {
    $requete = "select * from classe;";
    $con = connexion();
    if ($con != null)
    {
      $resultats = mysqli_query($con, $requete);
      // Parcours des résultats
      while ($ligne = mysqli_fetch_assoc($resultats))
      {
        echo "<option value = '".$ligne['idclasse']."'>".$ligne['designation']."</option>";
      }
      echo "</table>";
      deconnexion($con);
    }
  }

  function insert_class ($tab)
  {
    $requete = "insert into classe values (null, '".$tab['nom']."','".$tab['salle']."');";
    $con = connexion();
    if ($con != null)
    {
      mysqli_query($con, $requete);
      deconnexion ($con);
    }
  }

  function delete_class($idclasse)
  {
    $requete = "delete from classe where idclasse = ".$idclasse.";";
    $con = connexion();
    if($con != null)
    {
      mysqli_query($con, $requete);
      deconnexion($con);
    }
  }

  function eleves_classe ($idclasse)
      {
          $requete = "select * from etudiant where idclasse = ".$idclasse.";";
          $con = connexion();
          if ($con != null)
          {
              $resultats = mysqli_query($con, $requete);
                 echo "<table class=\"custom\" border =1>
                 <tr><thead><td>Id Etudiant</td> <td>Nom</td><td>Prenom</td><td>Email</td>
                 <td>Statut</td> <td>Date naissance</td></thead></tr>";
                 while ($Ligne = mysqli_fetch_assoc($resultats))
                 {
                     echo"<tr><td>".$Ligne['idetudiant']."</td>
                             <td>".$Ligne['nom']."</td>
                             <td>".$Ligne['prenom']."</td>
                             <td>".$Ligne['email']."</td>
                             <td>".$Ligne['statut']."</td>
                             <td>".$Ligne['date_nais']."</td></tr>";
                 }
                 echo"</table>";

              deconnexion($con);
          }
      }

  function professeur_classe($idclasse)
    {
        $requete ="select p.idprof,p.nom, p.prenom,p.email,p.diplome, e.annee from professeur p, enseigner e where p.idprof= e.idprof and e.idclasse = ".$idclasse.";";
        $con = connexion();
        if ($con != null)
        {
            $resultats = mysqli_query($con, $requete);
               echo "<table border =1>
               <tr><td>Professeur</td><td>Nom</td><td>Prenom</td>
               <td>Email</td><td>Diplome</td><td>Annee</td></tr>";
               while ($Ligne = mysqli_fetch_assoc($resultats))
               {
                   echo"<tr><td>".$Ligne['idprof']."</td>
                           <td>".$Ligne['nom']."</td>
                           <td>".$Ligne['prenom']."</td>
                           <td>".$Ligne['email']."</td>
                           <td>".$Ligne['diplome']."</td>
                           <td>".$Ligne['annee']."</td></tr>";
               }
               echo"</table>";

            deconnexion($con);
        }
      }

      function select_etudiants()
      {
          $requete = "select e.idetudiant, e.nom, e.prenom, e.email, e.statut, e.date_nais, c.designation from etudiant e, classe c where e.idclasse = c.idclasse;";
          $con = connexion();
          if ($con != null)
          {
              $resultats = mysqli_query($con, $requete);

                 echo "<table class=\"custom\" border =1>
                 <tr><thead><td>Id Etudiant</td> <td>Nom</td><td>Prenom</td><td>Email</td>
                 <td>Statut</td> <td>Date naissance</td><td>Classe</td></thead></tr>";
                 while ($Ligne = mysqli_fetch_assoc($resultats))
                 {
                     echo"<tr><td>".$Ligne['idetudiant']."</td>
                             <td>".$Ligne['nom']."</td>
                             <td>".$Ligne['prenom']."</td>
                             <td>".$Ligne['email']."</td>
                             <td>".$Ligne['statut']."</td>
                             <td>".$Ligne['date_nais']."</td>
                             <td>".$Ligne['designation']."</td></tr>";

                 }
                 echo"</table>";

              deconnexion($con);
          }
      }

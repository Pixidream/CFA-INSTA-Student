DROP database IF EXISTS cfa;
CREATE database cfa;
USE cfa;
create table classe (
    idclasse INT(5) NOT NULL auto_increment,
    designation VARCHAR(50),
    salle VARCHAR(50),
    PRIMARY KEY (idclasse)
  );

CREATE TABLE etudiant(
  idetudiant INT(5) NOT NULL auto_increment,
  email VARCHAR(100) NOT NULL,
  mdp VARCHAR(50) NOT NULL,
  nom VARCHAR(50),
  prenom VARCHAR(50),
  statut enum ("initial", "prof", "apprenti"),
  date_nais date,
  idclasse int(5) NOT NULL,
  PRIMARY KEY(idetudiant),
  FOREIGN KEY(idclasse) REFERENCES classe (idclasse)
);

CREATE TABLE matiere (
  idmatiere INT(5) NOT NULL auto_increment,
  libelle VARCHAR(50) NOT NULL,
  coef INT(2),
  PRIMARY KEY (idmatiere)
);

CREATE TABLE professeur (
  idprof INT(5) NOT NULL auto_increment,
  email VARCHAR(100) NOT NULL,
  mdp VARCHAR(20) NOT NULL,
  nom VARCHAR(50),
  prenom VARCHAR(50),
  diplome VARCHAR(50),
  PRIMARY KEY (idprof)
);



CREATE TABLE enseigner (
  idprof INT(5) NOT NULL,
  idclasse INT(5) NOT NULL,
  idmatiere INT(5) NOT NULL,
  annee VARCHAR(20) NOT NULL,
  PRIMARY KEY(idprof, idclasse, idmatiere, annee),
  FOREIGN KEY(idprof) REFERENCES professeur(idprof),
  FOREIGN KEY(idclasse) REFERENCES classe(idclasse),
  FOREIGN KEY(idmatiere) REFERENCES matiere(idmatiere)
);

CREATE TABLE avoir (
  idetudiant INT(5) NOT NULL,
  idmatiere INT(5) NOT NULL,
  datenote date,
  note FLOAT(2.2),
  PRIMARY KEY(idetudiant, idmatiere, datenote),
  FOREIGN KEY(idetudiant) REFERENCES etudiant(idetudiant),
  FOREIGN KEY(idmatiere) REFERENCES matiere(idmatiere)
);

-- Insertion des classes
INSERT INTO classe VALUES (NULL, "Promo 194", "Salle 5"), (NULL, "Promo 193", "Salle 3");

-- Insertion des Etudiants
INSERT INTO etudiant VALUES (NULL, "a@gmail.com", "123", "Paul", "Olivier", "Initial", "2000-02-02", 1), (NULL, "h@gmail.com", "465", "Mohamed", "Amine", "Prof", "1999-05-05", 2);

-- Insertion des Profs
INSERT INTO professeur VALUES (NULL, "p@gmail.com", "789", "Fred", "Launay", "Bac+5"), (NULL, "d@gmail.com", "789", "Damien", "Dupont", "Bac+4");

-- Insertion des matières
INSERT INTO matiere VALUES (NULL ,"Francais",2), (NULL ,"Maths",2);

-- Insertion des enseignements

INSERT INTO enseigner VALUES (1,1,1,"2018-2019"), (1,2,2,"2018-2019");

-- Insertion des notes

INSERT INTO avoir VALUES (1,1,"2018-12-12",15), (1,1,"2019-01-05",12),(2,1,"2018-12-12",11),(2,1,"2019-02-02",13);
